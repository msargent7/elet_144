; Source file: mss_Lab11a.a51
; Michael S. Sargent 4/5/17
; Interfacing DS12887 RTC to the 8051 Trainer
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to display time and date on a PC or LCD screen

Start:
		ORG 0000
			
;--------Serial Port Set-up
Main:
		MOV TMOD, #20H		;Serial port set up
		MOV SCON, #50H
		MOV TH1, #-3		;9600 baud
		SETB TR1			;Start timer 1
		CLR P1.7			;Turn off LED
;--------Turning on the RTC
		MOV R0, #10			;R0=0AH, Reg A address
		MOV A, #20H			;010 in D6-D4 to turn on osc.
		MOVX @R0, A			;send it to Reg A of DS12887
		
;-------Setting the time mode
		MOV R0, #11			;REG B address
		MOV A, #83H			;BCD, 24 hrs, daylight saving
		MOVX @R0, A			;send it to reg B
		
;--------Read time(HH:MM:SS) Convert it and display it
OV1:	MOV A, #20H			;ASCII for SPACE
		ACALL SERIAL
		MOV R0, #4			;point to HR location
		MOVX A, @R0			;read hours
		ACALL DISPLAY
		MOV A, #20H			;send out SPACE
		ACALL SERIAL
		MOV R0, #2			;point to minute location
		MOVX A, @R0			;read minute
		ACALL DISPLAY
		MOV A, #20H			;send out SPACE
		ACALL SERIAL
		MOV R0, #0			;point to seconds location
		MOVX A, @R0			;read seconds
		CJNE A, #25H, JUMP ;If seconds != 25 skip LED set
		SETB P1.7			;Turn LED on
JUMP:	CJNE A, #30H, JUMP2  ;IF seconds !=30 leave LED on
		CLR P1.7			;Turn LED off
JUMP2:
		ACALL DISPLAY

		
;--------Read Date (YYYY:MM:MM) Convert it and display it
		MOV A, #20H			;ASCII space
		ACALL SERIAL
		MOV A, #'2'			;Send out 2 (for 20)
		ACALL SERIAL
		MOV A, #'0'			;Send out 0 (for 20)
		ACALL SERIAL
		MOV R0, #09			;point to year location
		MOVX A, @R0			;read year
		ACALL DISPLAY
		MOV A, #':'			;Send out: for yyyy:mm
		ACALL SERIAL
		MOV R0, #08			;Point to month location
		MOVX A, @R0			;read month
		ACALL DISPLAY
		ACALL DELAY
		MOV A, #':'			;Send out : for mm:dd
		ACALL SERIAL
		MOV R0, #07			;point to day location
		MOVX A, @R0			;read day
		ACALL DISPLAY
		MOV A, #' '			;Send out space
		ACALL SERIAL
		ACALL DELAY
		MOV A, #0DH			;send LF
		ACALL SERIAL 
		ACALL DELAY
		LJMP OV1			;Display time and date forever
		
		
;--------Small Delay subroutine
DELAY:
		MOV R7, #250
D1:		DJNZ R7, D1
		RET

;--------Convert BCD to ASCII and send it to screen subroutine
DISPLAY:
		MOV B,A
		SWAP A
		ANL A, #0FH
		ORL A, #30H
		ACALL SERIAL
		MOV A, B
		ANL A, #0FH
		ORL A, #30H
		ACALL SERIAL
		RET
		
;--------Seraial subroutine
Serial:
		MOV SBUF, A
S1:		JNB TI, S1
		CLR TI
		RET	
		
		END