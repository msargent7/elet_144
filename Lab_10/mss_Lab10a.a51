; Source file: mss_Lab10a.a51
; Michael S. Sargent 4/5/17
; PWM with DC Permanent Magnet Motors
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to control the rotation rate of the DC motor 
;from an 8-bit switch using program interrupts.

Start:
		ORG 0000H			;Define program origination
		AJMP MAIN			;Jump to main to avoid intterupt vector
		
		SWITCH EQU P1		;Define meaningful labels
		MOTOR BIT P0.0
		
;--ISR for Timer 0 to intterupt after 156 clock cycles--
		ORG 000BH
T0ISR:	SJMP T0INT			;SJump to the interrupt definition
		RETI

;--ISR for Timer 1 to interrupt from the value on the 8-bit switch--
		ORG 001BH
T1ISR:	CLR MOTOR			;Turn the motor off
		RETI
		
		ORG 0040H			

MAIN:	
		MOV TMOD, #22H		;Set T0 and T1 to mode 2
		MOV TH0, #00H		;Set T0 to count to 256
		MOV IE, #10001010B	;Enable EA, T1 interrupt and T0 interrupt
		SETB TR0			;Start timer 0
		SJMP $;				;Stay here
		
T0INT:
		MOV TH0, #00H		;Set timer 0 to count to 256
		SETB TR0			;Start timer 0
		SETB MOTOR			;Start the motor
		MOV TH1, SWITCH		;Set timer 1 to count based off switch
		SETB TR1			;Start timer 1
		RETI
		
		END