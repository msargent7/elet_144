; Source file: mss_Lab06b.a51
; Michael S. Sargent 3/1/17
; Interfacing ADC0804 and Sensors with the LCD
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to input a varying voltage from a light senstive
;resistor into an ADC0804. That binary value will then be converted to an ASCII
;string in ram, and the 3 digits between 000 and 255 will be displayed on the
;LCD.


Start:

		ORG 0000H			;Define program origination
			
		ADC EQU P0			;Assign meaningful label to P0
		ASCII_ADDRESS	EQU 40H		
		
LCDOUT:	LCALL PWR_DELAY		;15 mSec delay to allow the LCD to boot
		LCALL INIT			;Initialize with time delays
		
BEGIN:	MOV A, #080H		;Set the address for where to display on the LCD
		LCALL CMD			;Send 80 as a command to the LCD
		ACALL GET_ADC		;Call the subroutine to get the ADC
		ACALL BINARY_TO_ASCII	;Converst to ASCII
		MOV R0, #ASCII_ADDRESS	;Set the DPTR to the ASCII string
		
		
LOOP:	MOV A, @R0		    ;Move along the ASCII string 
		JZ BEGIN			;End of the string is denoted with a 0. If 0
							;jump to the end of the program
		LCALL DAT			;Send the data to the screen
		INC R0				;Point to the next value in the ASCII string
		SJMP LOOP			;Repeat until you reach 0

		

; =============================================================================
;       Subroutine
;		BINARY_TO_ASCII
; -----------------------------------------------------------------------------
;INPUT - Binary number from P1
;OUTPUT - ASCII equivalent to the binary number input from P1
;This subroutine will read in a binary number from P1 to A. It will then
;convert this value to ASCII and store that value in RAM.

BINARY_TO_ASCII:
	
	MOV R0, #ASCII_ADDRESS+3
	
	MOV @R0, #0		;End a terminating zero to the string
	DEC R0			;Decrement R0 to store the next value
	MOV B, #10		;Set B to 10 for division
	DIV AB
	
	ORL B, #30H		;ORL the remainder with 30H to determine the ASCII value
	MOV @R0, B		;Store the ASCII value in R0
	DEC R0			;Decrement R0 to store the next value

	MOV B, #10		;Set B to 10 for division
	DIV AB
	
	ORL B, #30H		;ORL the remainder with 30H to determine the ASCII value
	MOV @R0, B		;Store the ASCII value in R0
	DEC R0			;Decrement R0 to store the next value
	ORL A, #30H		;ORL the final digit with 30H to determine the ASCII value
	MOV @R0, A		;Store the ASCII value in R0
		
	RET

; =============================================================================
;       Subroutine
;		GET_ADC
; -----------------------------------------------------------------------------
;INPUT - Analog data from an exterior device
;OUTPUT - Binary representation of the data that was read by the ADC
;This subroutine reads analog data from the a connected device. It then uses the
;ADC to convert this data from analog to digital, and returns a binary value
;to the main progam. This binary value is stored in A

GET_ADC:		
		M_RD	BIT P2.5
		M_WR	BIT P2.6
		INTR	BIT P2.7
			
		MOV ADC, #0FFH		;Set ADC ready for input
		SETB	INTR
		
		CLR M_WR			;WR = 0
		SETB M_WR			;Start the conversion with an L-to-H on WR
HERE:	JB INTR, HERE		;Wait for the conversion to end
		CLR M_RD			;Enable RD once the conversion is done
		NOP
		MOV A, ADC			;Read the data and store it in A		
		SETB M_RD			;Set RD for next loop

		RET
;***********************************************************************
; File: LCDOUT4_Subroutines.a51
; This file contains the 5 subroutines used by LCDOUT 
; DAT sends a data byte that is in A to the LCD with RS=1
; CMD sends a data byte that is in A to the LCD with RS=1
; INIT sends several bytes to the CMD subroutine for startup
; PWR_DELAY waits 15.218 mS for startup
; WAIT_LCD waits for the LCD Busy Flag to go low
; PWR_DELAY and WAIT_LCD updated 10/5/16 
;***********************************************************************

; IO Port and bits naming
  
LCD 	EQU P1		; Data lines on LCD connect to this IO Port
					; "LCD" may be changed to another port, if necessary
BZYFLG	BIT P1.7  	; Can read this from LCD to see if its still busy
					; ***Change this if data lines not on Port 1!!!***

RS 	BIT P2.0	; Set this control bit high to send data
RW	BIT P2.1	; Set this bit high to read (status) from LCD
EN 	BIT P2.2	; This is the clock into the LCD. Take high to ready 				
				; the LCD
				; Then take low to clock data or cmd onto the data 				
				; lines of LCD

;** DAT Subroutine. Sends Data byte to LCD because RS is High****
;	Input: ASCII character in register A 
; 	How it works: sets EN hi to prepare; sets RW low to write to LCD
;	RS is high to indicate that this is Data
; 	Places the data byte on Port "LCD" and gives clock the falling edge 
; 	on EN which loads one data byte to the LCD.
          
DAT:	SETB EN		; Prepare for H->L PULSE ON E 
	SETB RS			; RS=1 meand sending DATA
	CLR RW			; RW=0 for WRITE
	MOV LCD,A		; Place data byte onto data pins of LCD
	CLR EN			; Falling edge on EN pin writes data byte in A to LCD
	NOP				; To give a bit of time. Documentation suggested
	LCALL WAIT_LCD	; Allow LCD to digest data byte
	RET

;***** CMD Subroutine.Sends Command byte to LCD because RS is High****
;	Input: a command character in register A 
; 	How it works: sets EN hi to prepare; sets RW low to write to LCD
; 	RS is low to indicate to LCD that this is a command
; 	Places the command byte on Port "LCD" and gives clock the falling edge 
; 	on EN which loads one data byte to the LCD.
 
CMD:	SETB EN
	CLR RS
	CLR RW
	MOV LCD,A
    CLR EN
    NOP
    LCALL WAIT_LCD
	RET
        
;******Initialize LCD Subroutine****************************
; "The book" says we need to send 38H three times if there 
; are only 4 data lines being used from microcontroller to the LCD
        
INIT:	MOV A,#038H	; INITIALIZE, 2-LINES, 5X7 MATRIX.	
	LCALL  CMD
        	
	MOV A,#038H	; INITIALIZE, 2-LINES, 5X7 MATRIX.
	LCALL  CMD	; Need this 3 times if 4 data lines
        
	MOV A,#038H 	; INITIALIZE, 2-LINES, 5X7 MATRIX.
	LCALL  CMD	 	
                
	MOV A,#0EH		;  VISIBLE CURSOR ON;
	LCALL  CMD
	MOV A,#01H		; CLEAR LCD SCREEN
	LCALL  CMD
	MOV A,#06H		; SHIFT CURSOR RIGHT
	LCALL  CMD
	RET
                
;********PWR_Delay (long) Subroutine***************************
PWR_DELAY:	; Power on delay. 361.6896 nS * 42075 = 15.218 mSec
			; Maybe longer than needed but only once at startup.
           
		MOV R4,#165D
OUTER:	MOV R3,#255D
INNER:	DJNZ R3,INNER
		DJNZ R4,OUTER
        RET
    
;*******Wait for Busy Flag to be low Subroutine  ********
; Instead of waiting an estimated amount of time for LCD to be done
;  after sending a command or data byte, we can read the LCD's busy 
; flag on P1 pin 7 and wait for it to become zero. This is called by
; the CMD or DAT subroutines.

WAIT_LCD:	; We have sent something to th LCD. Now read the busy
			; flag to see if LCD is done (not busy)

	SETB BZYFLG	; enable input on busy flag bit. P1.7 
	CLR RS		; P2.0 low for read command
	SETB RW		; P2.1 high to read status
	CLR EN		; per Mazidi
	NOP			; leave EN low for a few cycles
	NOP
	NOP
	NOP
	SETB EN		; rising edge on EN reads
	
	MOV LCD, #0FFh	; Write command to LCD for Read status

	MOV A, LCD			;	Read the status from LCD
	JB ACC.7, WAIT_LCD 	; If ACC.7 high, LCD still busy
	CLR RW			; Return to write mode
	RET 

END