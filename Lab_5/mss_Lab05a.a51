; Source file: mss_Lab05a.a51
; Michael S. Sargent 2/15/17
; Addressing Modes and Table Lookups
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to read a binary value from a switch. The program
;then outputs the value of x cubed to the LEDS in binary, continuously. The cubed 
;value is obtained using a look-up table.

Start:
		ORG 0000H			;Define program origination
			
		Switch EQU P1	;Assign meaningful labels to P1 and P2
		LEDs EQU P2
			
		MOV DPTR, #200H		;Load up the table address
		MOV A, #0FFH		
		MOV P1, A			;Configure P1 for input
BACK:	MOV A, P1			;Store the input value
		ANL A, #07H			;Mask the input with 7 to ensure that it is
							;three bits or less
		CJNE A, #07H, NEXT	;Compare to see if the input is 7
		ACALL BAD_INPUT		;If the input is 7, call BAD_INPUT subroutine
		SJMP BACK			;Return to BACK to get a new input
NEXT:	MOVC A, @A+DPTR	;Get X cubed from the table
		MOV LEDs, A			;Display the cubed value on the LEDs
		SJMP BACK
		
		
			
	
; =============================================================================
;       Subroutine
;		BAD_INPUT
; -----------------------------------------------------------------------------
;INPUT - #07H
;OUTPUT - 11111111B and 00000000B alternatively on the LEDs
;Flash the 8 LEDs on and off every half second until the input value is changed.

		BAD_INPUT:

			MOV R0, A		;Store the original input to R0
			
			MOV A, #00H		;The following three lines display lit LEDs for
			MOV LEDs, A		;500 ms
			ACALL DELAY
			
			MOV A, #0FFH	;The following three lines display unlit LEDs
			MOV LEDs, A		;for 500 ms
			ACALL DELAY
			
			MOV A, R0		;Return the original input to A
			
				RET
			
; =============================================================================
;       Subroutine
;		DELAY
; -----------------------------------------------------------------------------
;Subroutine 1/2 second delay
		;The following subrouting creates a 1/2 second delay in the program
		;(90.42 ns * 4) = 361.69 ns. (
		;500,000 micro seconds / .36169 microseconds) = 1,382,399 loop iterations
		;(1,382,399 / (256 * 256 ) = 21.093. Round up to 22.
		;(1,382,399 / (256 * 22) = 245.45. Round down to 245.
		;Three nested loops will be needed with 22, 245 and 256 iterations respectively.
		
		DELAY: 
						MOV R0, #22
			OUTER:		MOV R1, #245
			MIDDLE:		MOV R2, #0
			INNER:		DJNZ R2, INNER
						DJNZ R1, MIDDLE
						DJNZ R0, OUTER

				RET
				
; =============================================================================
;       Look-up Table
;		Cubed numbers
; -----------------------------------------------------------------------------
;The look-up table below defines the valid x cubed values to be used in this program

		ORG 200H			;Define the table origination
			
XCUBED_TABLE:
		DB		0,1,8,27,64,125, 216	;Valid cubes for the table
		END
			
	