; Source file: mss_Lab08b.a51
; Michael S. Sargent 3/22/17
; 8051 Serial Interfacing
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to 
Start:
		ORG 0000H			;Define program origination
			
		SBUF1 EQU 0C1H
		SCON1 EQU 0C0H
		TI1 BIT 0C1H
		RI1 BIT 0C1H
			
LCDOUT:	ACALL PWR_DELAY		;15 mSec delay to allow the LCD to boot
		ACALL INIT			;Initialize with time delays
		MOV DPTR, #Message	
		ACALL T1_Setup		
		ACALL CMD_80		;Set the display address
		
		CLR P2.4			;Set up LEDs to check progress
		CLR P2.5
		CLR P2.6
		CLR P2.7
		
LOOP:	ACALL Xmt_Char		;Send a character
		CPL P2.4			;Progress check 
		ACALL Half_Sec		;Call for a .5 second delay
		CPL P2.5			;Progress check
		ACALL Recv_Char		;Get a character from SBUF1
		CPL P2.6			;Progress check
		ACALL Disp_Char		;Display the received character
		CPL P2.7			;Progress check
		SJMP LOOP

; =============================================================================
;       Subroutine
;		Xmt_Char
; -----------------------------------------------------------------------------
;INPUT - Character from ROM
;OUTPUT - Character to Serial 1
;This subroutine will get the next character from ROM and send it out to 
;Serial 1

		Xmt_Char:
		CLR A
		MOVC A, @A+DPTR		;Get the character to send
		INC DPTR			;Move to the next character
		MOV SBUF1, A		;Put character in SBUF
		JNB TI1, $			;Wait for the transmit flag
		CLR TI1				;Clear for next call
		JNZ Leave			;If not the end of the string, leave
		MOV DPTR, #Message	;If end of string, reload message
LEAVE:	RET

; =============================================================================
;       Subroutine
;		Half_Sec
; -----------------------------------------------------------------------------
;Subroutine 1/2 second delay
		;The following subrouting creates a 1/2 second delay in the program
		;(90.42 ns * 4) = 361.69 ns. (
		;500,000 micro seconds / .36169 microseconds) = 1,382,399 loop iterations
		;(1,382,399 / (256 * 256 ) = 21.093. Round up to 22.
		;(1,382,399 / (256 * 22) = 245.45. Round down to 245.
		;Three nested loops will be needed with 22, 245 and 256 iterations respectively.
		
		Half_Sec: 
						MOV R0, #22
			OUT:		MOV R1, #245
			MIDDLE:		MOV R2, #0
			IN:			DJNZ R2, IN
						DJNZ R1, MIDDLE
						DJNZ R0, OUT

				RET

; =============================================================================
;       Subroutine
;		Recv_Char
; -----------------------------------------------------------------------------
;INPUT - Character from SBUF
;OUTPUT - Character in A, reset RI flag
;This subroutine will wait for the RI flag to go high, signaling that a 
;value has been received in SBUF. Once that value is recieved, it will store the
;value in A, and reset the RI flag to await the next receive.

		Recv_Char:
			JNB RI1, $			;Wait for the RI flag to signal a character
								;has been received
			MOV A,SBUF1			;Store the character in A
			CLR RI1				;Clear the RI to be ready for the next recieve
			
			RET
			
; =============================================================================
;       Subroutine
;		Disp_Char
; -----------------------------------------------------------------------------
;INPUT - Stored character
;OUTPUT - Display character
;This subroutine will display the stored character on the LCD. If the stored 
;character is a null character, then it will call CMD_80

		Disp_Char:
			JZ Clear			;Check for a null character in the SBUF
			MOV SBUF, A			;Load the data
			ACALL DAT			;Send the character to the LCD
			
			SJMP Escape
Clear:		ACALL CMD_80		;Send the LCD to home if theres a null character
			ACALL Half_Sec		;Call for a 1/2 second delay
			CLR A			
Escape:			
			RET
			
; =============================================================================
;       Subroutine
;		T1_Setup
; -----------------------------------------------------------------------------
;INPUT - 
;OUTPUT - 
;This subroutine will set Timer 1 in Mode 2 to 9600 Baud, set SCON and SCON1,
;set TR1 and clear RI_1

		T1_Setup:
			MOV TMOD, #20H		;Set TMOD to timer 1, mode 2
			MOV TH1, #-3		;Set the baud rate to 9600
			MOV SCON1, #50H		;Set the SCON for 8-bit with a 1-bit stop
			SETB TR1			;Start timer 1
			CLR RI1				;Clear the receive flag
			
			RET
			
; =============================================================================
;       Subroutine
;		CMD_80
; -----------------------------------------------------------------------------
;INPUT - 
;OUTPUT - Set the address for where to display on the LCD
;This subroutine will send CMD 80 to the LCD

		CMD_80:
			MOV A, #080H		;Set the address for where to display on the LCD
			ACALL CMD
		
			RET

; =============================================================================
;       The message
; -----------------------------------------------------------------------------
MESSAGE:		DB "This is my message!",0	;Message to be displayed

;***********************************************************************
; File: LCDOUT4_Subroutines.a51
; This file contains the 5 subroutines used by LCDOUT 
; DAT sends a data byte that is in A to the LCD with RS=1
; CMD sends a data byte that is in A to the LCD with RS=1
; INIT sends several bytes to the CMD subroutine for startup
; PWR_DELAY waits 15.218 mS for startup
; WAIT_LCD waits for the LCD Busy Flag to go low
; PWR_DELAY and WAIT_LCD updated 10/5/16 
;***********************************************************************

; IO Port and bits naming
  
LCD 	EQU P1		; Data lines on LCD connect to this IO Port
					; "LCD" may be changed to another port, if necessary
BZYFLG	BIT P1.7  	; Can read this from LCD to see if its still busy
					; ***Change this if data lines not on Port 1!!!***

RS 	BIT P2.0	; Set this control bit high to send data
RW	BIT P2.1	; Set this bit high to read (status) from LCD
EN 	BIT P2.2	; This is the clock into the LCD. Take high to ready 				
				; the LCD
				; Then take low to clock data or cmd onto the data 				
				; lines of LCD

;** DAT Subroutine. Sends Data byte to LCD because RS is High****
;	Input: ASCII character in register A 
; 	How it works: sets EN hi to prepare; sets RW low to write to LCD
;	RS is high to indicate that this is Data
; 	Places the data byte on Port "LCD" and gives clock the falling edge 
; 	on EN which loads one data byte to the LCD.
          
DAT:	SETB EN		; Prepare for H->L PULSE ON E 
	SETB RS			; RS=1 meand sending DATA
	CLR RW			; RW=0 for WRITE
	MOV LCD,A		; Place data byte onto data pins of LCD
	CLR EN			; Falling edge on EN pin writes data byte in A to LCD
	NOP				; To give a bit of time. Documentation suggested
	LCALL WAIT_LCD	; Allow LCD to digest data byte
	RET

;***** CMD Subroutine.Sends Command byte to LCD because RS is High****
;	Input: a command character in register A 
; 	How it works: sets EN hi to prepare; sets RW low to write to LCD
; 	RS is low to indicate to LCD that this is a command
; 	Places the command byte on Port "LCD" and gives clock the falling edge 
; 	on EN which loads one data byte to the LCD.
 
CMD:	SETB EN
	CLR RS
	CLR RW
	MOV LCD,A
    CLR EN
    NOP
    LCALL WAIT_LCD
	RET
        
;******Initialize LCD Subroutine****************************
; "The book" says we need to send 38H three times if there 
; are only 4 data lines being used from microcontroller to the LCD
        
INIT:	MOV A,#038H	; INITIALIZE, 2-LINES, 5X7 MATRIX.	
	LCALL  CMD
        	
	MOV A,#038H	; INITIALIZE, 2-LINES, 5X7 MATRIX.
	LCALL  CMD	; Need this 3 times if 4 data lines
        
	MOV A,#038H 	; INITIALIZE, 2-LINES, 5X7 MATRIX.
	LCALL  CMD	 	
                
	MOV A,#0EH		;  VISIBLE CURSOR ON;
	LCALL  CMD
	MOV A,#01H		; CLEAR LCD SCREEN
	LCALL  CMD
	MOV A,#06H		; SHIFT CURSOR RIGHT
	LCALL  CMD
	RET
                
;********PWR_Delay (long) Subroutine***************************
PWR_DELAY:	; Power on delay. 361.6896 nS * 42075 = 15.218 mSec
			; Maybe longer than needed but only once at startup.
           
		MOV R4,#165D
OUTER:	MOV R3,#255D
INNER:	DJNZ R3,INNER
		DJNZ R4,OUTER
        RET
    
;*******Wait for Busy Flag to be low Subroutine  ********
; Instead of waiting an estimated amount of time for LCD to be done
;  after sending a command or data byte, we can read the LCD's busy 
; flag on P1 pin 7 and wait for it to become zero. This is called by
; the CMD or DAT subroutines.

WAIT_LCD:	; We have sent something to th LCD. Now read the busy
			; flag to see if LCD is done (not busy)

	SETB BZYFLG	; enable input on busy flag bit. P1.7 
	CLR RS		; P2.0 low for read command
	SETB RW		; P2.1 high to read status
	CLR EN		; per Mazidi
	NOP			; leave EN low for a few cycles
	NOP
	NOP
	NOP
	SETB EN		; rising edge on EN reads
	
	MOV LCD, #0FFh	; Write command to LCD for Read status

	MOV A, LCD			;	Read the status from LCD
	JB ACC.7, WAIT_LCD 	; If ACC.7 high, LCD still busy
	CLR RW			; Return to write mode
	RET 

END