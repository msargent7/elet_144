; Source file: mss_Lab08a.a51
; Michael S. Sargent 3/22/17
; 8051 Serial Interfacing
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to send a text message to Terra Term on the PC
;using serial Port 0.(USB) The message will be "ELET 144C is power! <CR>,<LF>"
;and will be stored in the flash program store. The message will be terminated
;by a null byte that serves as end of message indicator.

Start:
		ORG 0000H			;Define program origination
			
		MOV TMOD, #20H		;Set TMOD for timer 1, mode 2
		MOV TH1, #0FAH		;Set the baud rate at 4800
		MOV SCON, #50H		;Set the SCON for 8-bit, 1 stop, REN enabled
		SETB TR1			;Start timer 1
		MOV DPTR, #MESSAGE	;Load the pointer with a message
		
Here:	CLR A
		MOVC A, @A+DPTR		;Get the first character of the message
		ACALL SEND			;Send the character
		INC DPTR			;Incremement DPTR to get the next character
		JZ Finish
		SJMP Here			;Repeat the loop

Finish:	SJMP $
	
; =============================================================================
;       Subroutine
;		Half_Sec
; -----------------------------------------------------------------------------
;Subroutine 1/2 second delay
		;The following subrouting creates a 1/2 second delay in the program
		;(90.42 ns * 4) = 361.69 ns. (
		;500,000 micro seconds / .36169 microseconds) = 1,382,399 loop iterations
		;(1,382,399 / (256 * 256 ) = 21.093. Round up to 22.
		;(1,382,399 / (256 * 22) = 245.45. Round down to 245.
		;Three nested loops will be needed with 22, 245 and 256 iterations respectively.
		
		Half_Sec: 
						MOV R0, #22
			OUTER:		MOV R1, #245
			MIDDLE:		MOV R2, #0
			INNER:		DJNZ R2, INNER
						DJNZ R1, MIDDLE
						DJNZ R0, OUTER

				RET

		
; =============================================================================
;       Subroutine
;		SEND
; -----------------------------------------------------------------------------
;INPUT - Stored character
;OUTPUT - Display character
;This subroutine will display the stored character on the LCD. If the stored 
;character is a null character, then it will call CMD_80

		SEND:
			MOV SBUF, A			;Load the data
			JNB TI, $			;Wait here until the TI flag goes high
			ACALL Half_Sec		;Call for a 1/2 second delay
			CLR TI				;Clear the TI flag for the next call
			
			RET

; =============================================================================
;       The message
; -----------------------------------------------------------------------------
MESSAGE:		DB "ELET 144C is power!",0DH,0AH,0	;Message to be displayed
		

END