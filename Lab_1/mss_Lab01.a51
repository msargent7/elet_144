; Source file: mss_Lab01B.a51
; Michael S. Sargent 1/18/17
; First lab program to introduce Keil uVision 5
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;   
; A simple loop is executed.   The accumulator is cleared and the Carry flag
; is set (made 1).  Register R1 is used as a loop counter, set equal to 3.
; The loop (executed 3 times) rotates the accumulator with the Carry Flag to
; the left.
; Then a new loop is executed the maximum number of times and one more instruction
; follows before starting over.

Start:
		MOV R1, #10		; Initialize R1 as loop counter
		CLR A		 	; clear accumulator (make 0)
		SETB C			; set Carry Flag (make C=1)

		MOV R3, #03 		; Move 03 to register 3
		MOV A, #77h
		MOV 50h, A		; Move 77 hex to RAM address 50h
		CLR A

		MOV B, #0Fh		; Move 00001111 to register B

loop:
		RLC A			; rotate acc left through carry flag
		DJNZ R1, loop		; repeat 3 times
					; --- loop ends here -------------
		MOV R0, #0		; Set a different loop counter to zero

loop2:	DJNZ R0, loop2	; How many times will it loop
		INC R2			; Something to do after loop
		
		SJMP Start		; repeat the entire program...
					; ... this is an endless loop!
		END			; ASSEMBLER DIRECTIVE says no more code
		  
; --- end of file WLF_lab01B.a51 ------