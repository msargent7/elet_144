; Source file: mss_Lab02C.a51
; Michael S. Sargent 1/25/17
; MOV - SUM
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code assigns labels to decimal, binary and hex values. It also defines
;a string of ASCII numbers and ASCII characters, again using labels.
;The use of labels is then demonstrated by summing three different values that
;are defined using labels.

Start:
		ORG 50H					;Define origin for the following commands
			
	TEN:	 DB 1010B			;Defines TEN as 10 using binary
	TWENTY:	 DB 20				;Defines TWENTY as 20 using decimal
	THIRTY:	 DB 1DH				;Defines THIRTY as 30 using hex
	String1: DB "1234"			;Defines 1234 to print as the ASCII numbers 1234
		
	Text1:	 DB "Professor French loves ELET 144C!"	
								;Defines the previous statement to print
								;as ASCII characters
		
		ORG 0000H				;Define the origin for the following commads
		ADD A, #TEN				;Add 10 to the accumulator
		ADD A, #TWENTY			;Add 20 to the accumulator
		ADD A, #THIRTY			;Add 30 to the accumulator
		MOV B, A				;Move the sum from the accumulator to b
		
		DONE: SJMP DONE			;Block program from going to unwanted territory
		END						;End program