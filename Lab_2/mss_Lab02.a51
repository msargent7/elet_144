; Source file: mss_Lab02A.a51
; Michael S. Sargent 1/25/17
; MOV - SUM
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code moves the number 153 in to A, and registers R0-R7.
;The value is moved to A using the hex value, and then moved from A to R0-R4.
;The value is moved in to R5 using the decimal value, R6 using the binary value,
;and R7 using the hex value.


Start:
		MOV A, #99H			;Move 99 hex to A
		MOV R0, A			;Move A (99 hex) to R0
		MOV R1, A			;Move A (99 hex) to R1
		MOV R2, A			;Move A (99 hex) to R2
		MOV R3, A			;Move A (99 hex) to R3
		MOV R4, A			;Move A (99 hex) to R4
		
		MOV R5, #153D		;Move 153 decimal to R5
		MOV R6, #10011001B	;Move 153 binary to R6
		MOV R7,	#99H		;Move 99 hex to R7
		
		DONE: SJMP DONE		;Block program from going to unwanted territory
		END					;End the program