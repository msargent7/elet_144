; Source file: mss_Lab02B.a51
; Michael S. Sargent 1/25/17
; MOV - SUM
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code determines the sum of 7 given numbers, by adding one number at
;a time to the accumulator.
;This sum is then moved from the accumulator to R2.


Start:
		ADD A, #1D		;Add 1 to the accumulator
		ADD A, #8D		;Add 8 to the accumulator
		ADD A, #9D		;Add 9 to the accumulator
		ADD A, #2D		;Add 2 to the accumulator
		ADD A, #3D		;Add 3 to the accumulator
		ADD A, #1D		;Add 1 to the accumulator
		ADD A, #7D		;Add 7 to the accumulator
		
		MOV R2, A		;Move the contents of the accumulator to R2
		
		DONE: SJMP DONE	;Block program from going to unwanted territory
		END				;End of program