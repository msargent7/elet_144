; Source file: mss_Lab03a.a51
; Michael S. Sargent 2/1/17
; Using the CY, STACK and Subroutines
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to add five hex values, resulting in a 16-bit 
;value to be stored in B and ACC. This will be done to observe the change in
;the CY flag as these values are summed.

Start:
		ORG 0000H		;Define program origination
	
		CLR A			;Clear A to ensure there are no left over values
						;to interfere with the addition
		
		ADD A, #92H		
		ADD A, #48H	
		ADD A, #66H
		XCH A, B		;Store the value of A in B
		JNC N_1			;If there is no carry at this point, skip the ADDC
						;and continue to the next ADD
		ADDC A, #0		;If there is a carry, move the carry in to A
N_1:	XCH A, B		;Store the carry value in B, and return stored value of A
						;to A
		ADD A, #87H		
		XCH A, B		;Store the value of A in B, and move the carry value to A
		JNC N_2			;If there is no carry at this point, skip the ADDC
		ADDC A, #0		;Add any new carry value to the existing carry value in A
N_2:	XCH A, B		;Store the carry value in B, and return stored value of A
						;to A
		ADD A, #0F5H
		XCH A, B		;Store the value of A in B, and move the carry value to A
		ADDC A, #0		;Add any new carry value to the existing carry value in A
		

		
		
		
		DONE: SJMP DONE	;Block program from going to unwanted territory
		END				;End of program