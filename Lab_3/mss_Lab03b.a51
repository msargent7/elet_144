; Source file: mss_Lab03b.a51
; Michael S. Sargent 2/1/17
; Using the CY, STACK and Subroutines
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to load four different immediate values into
;each of the registers R0-R3, and then PUSH these values on to the stack. 
;The Program will then POP each of these values into registers R4-R7.

Start:
		ORG 0000H		;Define program origination
		
		MOV R0, #92H		;The following four lines load four unique
		MOV R1, #48H		;hex values to R0-R3
		MOV R2, #66H
		MOV R3, #87H
		
		PUSH 0		;SP = 08, Top = 92H
		PUSH 1		;SP = 09, Top = 48H
		PUSH 2		;SP = 0A, Top = 66H
		PUSH 3		;SP = 0B, Top = 87H
		
		POP 4		;R4 = 87H, SP = 0A, Top = 66H
		POP 5		;R5 = 66H, SP = 09, Top = 48H
		POP 6		;R6 = 48 H, SP = 08, Top = 92H
		POP 7		;R7 = 92H, SP = 07, Top = Undefined
					
					;The values are popped in a last in first out order
		
		DONE: SJMP DONE	;Block program from going to unwanted territory
		END				;End of program