; Source file: mss_Lab03c.a51
; Michael S. Sargent 2/1/17
; Using the CY, STACK and Subroutines
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to use a subroutine to add a 1/2 second delay to the program.
;The 1/2 second delay is created using a subroutine, so it can be called with a single
;command as many  times as needed.

Start:
		ORG 0000H		;Define program origination
		
Begin:	MOV R7, #55H
		ACALL DELAY		;Call four a 1/2 second delay using the DELAY subroutine
		
		MOV	R7, #0AAH
		ACALL DELAY		;Call four a 1/2 second delay using the DELAY subroutine
		
		SJMP Begin
		;Subroutine 1/2 second delay
		;The following subrouting creates a 1/2 second delay in the program
		;(90.42 ns * 4) = 361.69 ns. (
		;500,000 micro seconds / .36169 microseconds) = 1,382,399 loop iterations
		;(1,382,399 / (256 * 256 ) = 21.093. Round up to 22.
		;(1,382,399 / (256 * 22) = 245.45. Round down to 245.
		;Three nested loops will be needed with 22, 245 and 256 iterations respectively.
		
		DELAY: 
						MOV R0, #22
			OUTER:		MOV R1, #245
			MIDDLE:		MOV R2, #0
			INNER:		DJNZ R2, INNER
						DJNZ R1, MIDDLE
						DJNZ R0, OUTER
				RET
		END				;End of program