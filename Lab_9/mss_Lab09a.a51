; Source file: mss_Lab09a.a51
; Michael S. Sargent 4/5/17
; Stepper Motors
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to interface a 5V DC stepper motor with the
;MDE 8051 Trainer. The stepper motor will be controlled via the 8-bit switch
;that is located on the trainer.

		SWITCH EQU P1
		STEP   EQU P2
		Tms	   EQU 80H
		RotateBit	EQU P0.0

Start:
		ORG 0000H			;Define program origination
		AJMP MAIN
		
;--ISR for Timer 0 to increment Tms--
		ORG 000BH
T0ISR:	INC Tms
		RETI
			
		ORG 0040H	
MAIN:	
		
		
		MOV R0, #66H		;Load the step sequence
		MOV	TMOD, #2H		;Set timer 0, mode 2
		SETB IE.1			;Enable T0
		SETB IE.7			;Enable EA
		SETB TR0			;Start T0
		MOV TH0, #0A4H		;Set TH for 0.1 mS
							;256-164=92
							;92*1.085uS = 99.82uS
		
Back:	MOV A, SWITCH		;Mov Tms in to A to compare
		CJNE A, Tms, Back	;Keep looping until Tms = SWITCH
		MOV Tms, #0H		;Reset Tms when it matches SWITCH
		MOV A, R0			;MOV saved bit pattern in to A
		ACALL Rotate_Direct	;Subroutine Rotate_Direct determines motor direction
		MOV STEP, A			;Issue sequence to motor
		MOV R0, A			;Store bit pattern in R0
		SJMP Back
		
		
		
; =============================================================================
;       Subroutine
;		Rotate_Direct
; -----------------------------------------------------------------------------
;INPUT - Value from P0.0 to determine direction
;OUTPUT - Direction to rotate
;This subroutine will determine which direction to rotate the stepper motor

Rotate_Direct:
			JB RotateBit, RIGHT	;If the bit is HIGH, jump to rotate right	
			RL A				;If bit is LOW, rotate left
			SJMP Leave			;Leave after setting rotate
RIGHT: RR A						;Rotate right
LEAVE: RET
				
END