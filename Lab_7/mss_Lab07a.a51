; Source file: mss_Lab07a.a51
; Michael S. Sargent 3/8/17
; Timer and Counter Programming - Program 1
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to generate a variable frequency on one of the 
;pins of P1 using Timer 1 in mode 01. A value will be read from the 8 bit switch
;and moved into both T1H and T1L. This will allow the widest possible range of
;frequencies selectable with the switch.

Start:
		SWITCH EQU P0			;Assign meaningful label to P0
			
		ORG 0000H
			
		MOV SWITCH, #0FFH		;Set P0 for input
		MOV TMOD, #10H			;Timer 1, mode 1
		
Again:	MOV A, SWITCH			;Input the binary number from P0 to A 
		MOV TL1, A				;Move the value of A in to TL
		MOV TH1, A				;Move the value of A in to TH
		SETB TR1				;Start Timer 1
		CPL P1.5

Back:	JNB TF1, Back			;Stay until timer rolls over
		CLR TR1					;Stop Timer 1
		CLR TF1
		SJMP Again
		
		END
		