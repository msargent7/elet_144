; Source file: mss_Lab04b.a51
; Michael S. Sargent 2/8/17
; Using 8051 I/O Ports
; =============================================================================
;       m a i n    p r o g r a m
; -----------------------------------------------------------------------------
;
;The following code is designed to write FFH to P1. The switch connected to P1 will
;then be read as two 4-bit binary nibbles. After seperating the nibbles into the low
;bits of A and B, the nibbles will be swapped and added. The sum will then be sent 
;to the LEDs on P2 as a binary number.

Start:
		ORG 0000H		;Define program origination
		
		Switch EQU P1	;Assign meaningful labels to P1 and P2
		LEDs EQU P2
			
			
		MOV A, #0FFH	;Fill A with binary 1s
		MOV Switch, A	;Move A to P1

Begin:
		MOV A, Switch
		ANL A, #0FH		;AND A with 000111 to change the value of A
		MOV B, A		;Move the value of A to B
		MOV A, Switch
		SWAP A
		ANL A, #0FH
		ADD A, B		;Determine the sum of A and B
		MOV LEDs, A		;Send the sum to the LEDs
		
		SJMP Begin	;Prevent the program from going in to unwanted territory
		END
		